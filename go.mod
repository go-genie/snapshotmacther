module gitee.com/go-genie/snapshotmacther

go 1.21

require (
	github.com/onsi/gomega v1.10.1
	github.com/sergi/go-diff v1.1.0
)

require (
	golang.org/x/net v0.0.0-20200602114024-627f9648deb9 // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/xerrors v0.0.0-20191204190536-9bdfabe68543 // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
